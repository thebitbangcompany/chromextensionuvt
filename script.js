projects = [];
projectsJson = {};

function handleRequest(request){
	if (request.callFunction == "toggleSidebar") {
		toggleSidebar(request.url);
		projects = [];
	}

}

chrome.extension.onRequest.addListener(handleRequest);
var sidebarOpen = false;
console.log("sidebarOpen: ", sidebarOpen);

function toggleSidebar(tabUrl) {
	//var data = '{"test": "ok"}';
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'http://localhost:3000/collectionapi/project', true);
	// xhr.responseType = 'blob';
	xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
	xhr.onload = function(e) {
	  console.log("this.response: ", this.responseText);
	  sideBar(this.responseText, tabUrl, xhr);
	};
	xhr.send();
}

function insertD() {
	var data = JSON.stringify({test:'ok'});
	var XHR = new XMLHttpRequest();
	var FD  = new FormData();

  	// We push our data into our FormData object
	for(name in data) {
	    FD.append(name, data[name]);
	}

	console.log("FD: ", FD);
	// We define what will happen if the data is successfully sent
	XHR.addEventListener('load', function(event) {
	alert('Yeah! Data sent and response loaded.');
	});

	// We define what will happen in case of error
	XHR.addEventListener('error', function(event) {
	alert('Oups! Something goes wrong.');
	});

	// We setup our request
	XHR.open('POST', 'http://localhost:3000/collectionapi/searches', true);

	// We add the required HTTP header to handle a form data POST request
	XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	//XHR.setRequestHeader('Content-Length', urlEncodedData.length);

	XHR.onreadystatechange = function() {
	    if (XHR.readyState == 4 && XHR.status == 200) {
	    	console.log("this.response: ", this.responseText);
	    } else {
	    	console.log("this.response: ", this.responseText);
	    }
	};

	// And finally, We send our data.
	XHR.send(data);
}

function sideBar (response, tabUrl, XHR){

	var _response = JSON.parse(response);
	projectsJson = response;
	console.log(_response.length);

	for (var i = 0; i < _response.length; i++) {
		projects.push(_response[i]);
	};

	console.log("Projects: ", projects);

	if(sidebarOpen) {
		var el = document.getElementById('mySidebar');
		el.parentNode.removeChild(el);
		sidebarOpen = false;
	}
	else {
		var sidebar = document.createElement('div');
		var input = createInpunt("label", "name", "placeholder", "value");
		var textarea = createTextArea("Comment", "comment", "placeholder", "value");
		var options = createSelect("score", "Score this source",[{value:1, label:"Uno"}, {value:2, label:"Dos"}, {value:3, label:"Tres"}, {value:4, label:"Cuatro"}, {value:5, label:"Cinco"}], "score", "label-exc");
		sidebar.id = "mySidebar";
		sidebar.innerHTML = getInnerHTMLBase(input, textarea, options, projects, tabUrl, XHR);
		document.body.appendChild(sidebar);
		sidebarOpen = true;
		var scripts = getScripts(XHR);
		var myScript = document.createElement('script');
		myScript.id= "myScript";
		myScript.textContent = scripts;
		(document.head||document.documentElement).appendChild(myScript);
		myScript.parentNode.removeChild(myScript);

	}
}

function getProjectID(id){
	return console.log("ID: ", id);
}
 
function getScripts(XHR){
	// var form = 'var form = (function () { var form = document.getElementById("name").value();}());'
	var selectClass = "selection-exc";
	var proJson = projectsJson;
	var labelClass = "label-exc";
	var selectId = "selectid";
	var input = ["input-exc", "text"];
	var inputTextArea = ["textarea-exc", "40", "8"];
	var options = [{value:"Uno", label:"Uno"}, {value:"Dos", label:"Dos"}];

	var sendData = "function sendData(data, cb) {\
		var data = JSON.stringify(data);\
		var XHR = new XMLHttpRequest();\
		var FD  = new FormData();\
		for(name in data) {\
		    FD.append(name, data[name]);\
		}\
		XHR.addEventListener('load', function(event) {\
			console.log('Yeah! Data sent and response loaded.', event);\
			if( typeof cb === 'function' )\
                cb(true);\
		});\
		XHR.addEventListener('error', function(event) {\
			console.log('Oups! Something goes wrong.', event);\
			if( typeof cb === 'function' )\
                cb(false);\
		});\
		XHR.open('POST', 'http://localhost:3000/collectionapi/searches', true);\
		XHR.send(data);\
	}";

	var getElementsValues = 'function send(form){\
					var result = {};\
					var fields = [];\
					var send = true;\
					var inputs = form.getElementsByTagName("input");\
					var textareas = form.getElementsByTagName("textarea");\
					var selects = form.getElementsByTagName("select");\
					for (var i = 0; i < inputs.length; i++) {\
						console.log("e:", inputs[i].value, inputs[i].name);\
						if(inputs[i].value === ""){\
							send = false;\
							document.getElementById(inputs[i].name).classList.add("empty-exc");\
						} else {\
							if(inputs[i].name ===  "projectId" || inputs[i].name ===  "serviceId" || inputs[i].name ===  "tabUrl"){\
								result[inputs[i].name] = inputs[i].value;\
							} else {\
								var data = {};\
								data["id"] = inputs[i].name;\
								data["value"] = inputs[i].value;\
								data["type"] = "input";\
								fields.push(data);\
							}\
						}\
					};\
					for (var i = 0; i < textareas.length; i++) {\
						console.log("t:", textareas[i].value, textareas[i].name);\
						if(textareas[i].value === ""){\
							send = false;\
							document.getElementById(textareas[i].name).classList.add("empty-exc");\
						} else {\
							if(textareas[i].name ===  "comment"){\
								result[textareas[i].name] = textareas[i].value;\
							} else {\
								var data = {};\
								data["id"] = textareas[i].name;\
								data["value"] = textareas[i].value;\
								data["type"] = "textarea";\
								fields.push(data);\
							}\
						}\
					};\
					for (var i = 0; i < selects.length; i++) {\
						console.log("s:", selects[i].value, selects[i].name);\
						if(selects[i].value === "Select one option"){\
							send = false;\
							document.getElementById(selects[i].name).classList.add("empty-exc");\
						} else {\
							if(selects[i].name ===  "score"){\
								result[selects[i].name] = selects[i].value;\
							} else {\
								var data = {};\
								data["id"] = selects[i].name;\
								data["value"] = selects[i].value;\
								data["type"] = "select";\
								fields.push(data);\
							}\
						}\
					};\
					result["fields"] = fields;\
					if(send){\
						sendData(result, function(response){\
							console.log("Response callback", response);\
							if(response){\
								document.getElementById("comment").classList.remove("empty-exc");\
								document.getElementById("score").classList.remove("empty-exc");\
								for (var i = 0; i < result.fields.length; i++) {\
									var id = result.fields[i].id;\
									document.getElementById(id).classList.remove("empty-exc");\
									document.getElementById(id).value = "";\
								}\
								alert("The data was successfully sent");\
							} else {\
								alert("It was not possible to send the data. Please try again");\
							}\
						});\
					} else {\
						alert("The following fields are empty");\
					}\
				}';
	var createInpunt = 'function createInpunt(_label, _name, _placeholder){\
							var input = "<div>\
						    				<label class=' + labelClass + '>" + _label + "</label>\
											<input id=" + _name + " class=' + input[0] + ' type=' + input[1] + ' name=" + _name + " placeholder=" + _placeholder + " required>\
										</div>";\
							return input;\
						}';
	var createInpuntTextArea = 'function createInpuntTextArea(_label, _name, _value){\
									var inputArea = "<div>\
									    				<label class=' + labelClass + '>" + _label + "</label>\
														<textarea id=" + _name + " class=' + inputTextArea[0] + ' cols=' + inputTextArea[1] + ' rows=' + inputTextArea[2] + ' name=" + _name + " required></textarea>\
													</div>";\
									return inputArea;\
								}';
	var createInputSelect = 'function createInputSelect(label, name, options){\
								var opt = "<option>Select one option</option>";\
								for (var i = 0; i < options.length; i++) {\
									opt = opt + "<option value=" + options[i]._id + ">" + options[i].name + "</option>";\
								}\
								var select = "<div>\
							    				<label class=' + labelClass + '>" + label + "</label>\
							    				<select id=" + name + " name=" + name + " class=' + selectClass + ' required>" + opt + "</select>\
											</div>";\
								return select;\
							}';
	var createProjectSelector = 'function createSelect(name, label, options, id){\
							var result = [];\
							var opt = "<option>Select one option</option>";\
							for (var i = 0; i < options.length; i++) {\
								if(options[i]._id === id){\
									console.log("projects: ", options[i]);\
									var services = [];\
									for (var j = 0; j < options[i].services.length; j++) {\
										services.push(options[i].services[j]);\
										console.log("services: ", options[i].services[j]);\
										opt = opt + "<option value=" + options[i].services[j]._id + ">" + options[i].services[j].name + "</option>";\
									};\
								}\
							}\
							result.push(opt);\
							result.push(services);\
							var select = "<div>\
						    				<label class=' + labelClass + '>" + label + "</label>\
						    				<select id=' + selectId + ' class=' + selectClass + '>" + opt + "</select>\
										</div>";\
							return result;\
						}';

	var getProjectID = 'function getProjectID(id, projects){\
							var selectDiv = createSelect("name", "label", projects, id);\
							document.getElementById("projectsId").value = id;\
							console.log("service: ", selectDiv[1], id);\
							document.getElementById("selectid").innerHTML=selectDiv[0];\
							return selectDiv[1];\
						};';
	var getServices = 'services = "";\
						function getServiceID(id, services){\
							console.log("services...", services, id);\
							document.getElementById("serviceId").value = id;\
							document.getElementById("button-exc").style.display = "block";\
							for (var i = 0; i < services.length; i++) {\
								if(services[i]._id === id){\
									var fields = services[i].fields;\
									var input = "";\
									for (var j = 0; j < fields.length; j++) {\
										if(fields[j].status){\
											if(fields[j].type === "Text Short" || fields[j].type === "Date"){\
												var name = fields[j].name;\
												input = input + createInpunt(name, fields[j].id, fields[j].type);\
											}\
											if(fields[j].type === "Text Long"){\
												var name = fields[j].name;\
												input = input + createInpuntTextArea(name, fields[j].id, fields[j].type);\
											}\
											if(fields[j].type === "List"){\
												var name = fields[j].name;\
												input = input + createInputSelect(name, fields[j].id, fields[j].options);\
											}\
											document.getElementById("inputId").innerHTML=input;\
										}\
									};\
								}\
							};\
						}';
	var selectProjects = 'selectProjects = document.getElementById("select"); selectProjects.addEventListener("change", function() {services = getProjectID(selectProjects.value,' + proJson + ');});';
	var selectServices = 'selectServices = document.getElementById("selectid"); selectServices.addEventListener("change", function() {getServiceID(selectServices.value, services);});';
	var selection = 'var sele = (function () { ' + getServices + getProjectID + createProjectSelector + createInpunt + createInpuntTextArea + createInputSelect + '; ' + selectProjects + selectServices +' }());'
	var toggle = 'var toggle = (function () { var visible = false, ele = document.getElementById("mymenu"), btn = document.getElementById("menuBarToggle");function flip() {var display = ele.style.display;ele.style.display = (display === "block" ? "none" : "block");visible = !visible;}btn.addEventListener("click", flip);ele.addEventListener("click", function (e) {e.stopPropagation();});document.addEventListener("click", function (e) {if (visible && e.target !== btn) flip();});ele.style.display = "none";return flip;}());';
	var save = 'var save = (function () { ' + sendData + getElementsValues + '; checkPageButton = document.getElementById("button-exc");checkPageButton.addEventListener("click", function(e) {e.preventDefault(); send(document.forms["form-exc"]);});}());';
	var slideBar = 'var slideBar = (function () {ele1 = document.getElementById("mySidebar"), btn1 = document.getElementById("sideBarToggle");function slideInOut() {var webkitTransform = ele1.style.webkitTransform;ele1.style.webkitTransform = (webkitTransform === "translateX(275px)" ? "translateX(0px)" : "translateX(275px)");}btn1.addEventListener("click", slideInOut);ele1.addEventListener("click", function (e) {e.stopPropagation();});return slideInOut;}());';
	
	var scripts = [selection + toggle + save + slideBar].join('\n');
	//console.log("vars: ", scripts);

	return scripts;

}

function getMenu(projects){
	var prj = [];
	var ser = [];
	for (var i = 0; i < projects.length; i++) {
		var p = {value: projects[i]._id, label: projects[i].title}
		prj.push(p);
		// for (var j = 0; j < projects[i].services.length; j++) {
		// 	var s = {value: projects[i].services[j]._id, label: projects[i].services[j].name};
		// 	ser.push(s);
		// };
		
		console.log("projects menu: ", prj);
	};

	var projects = createSelect("projects", "Projects", prj, "select", "menu-label-exc");
	// var services = createSelect("services", "Services", ser);
	return projects;
}

function getInnerHTMLBase(input, textarea, options, projects, tabUrl, XHR){
	var menuImage = chrome.extension.getURL('menu.png');
	var menuImage1 = chrome.extension.getURL('sample.jpg');
	var sideBarImage = chrome.extension.getURL('Sidebar-Menu.png');
	var style = getStyle();
	var innerHTML = "\
		" + style + "\
		<div class='header-exc'><a href='javascript:void(0);'><img src='" + menuImage + "' id='menuBarToggle' class='menuBarToggle-exc'></a>\
        </div>\
			<a href='javascript:void(0);' ><img src=' " + sideBarImage + "' id='sideBarToggle' class='sideBarToggle-exc'></a>\
			<div id='mymenu' class='menu-exc'>\
			 " + getMenu(projects) + "\
				 <div id='projectId' style='display:block;'>\
					 <div>\
						<label class='menu-label-exc'>Services</label>\
						<select id='selectid' class='selection-exc'>\
							<option value='0'>None</option>\
						</select>\
					 </div>\
				 </div>\
            </div>\
			<div id='container-exc' class='container-exc'>\
				<form id='form-exc' action='javascript:void(0);' onSubmit='sendValuesToUVT()'>\
					<input id='projectsId' name='projectId' style='display:none;' value=''>\
					<input id='serviceId' name='serviceId' style='display:none;' value=''>\
					<input id='tabUrl' name='tabUrl' style='display:none;' value=" + tabUrl + ">\
					<div id='inputId'></div>\
					" + textarea + "\
					" + options + "\
					<button id='button-exc' class='button-exc' type='button' onClick='return false;' style='display:none!Important;'>Save</button>\
				</form>\
			</div>\
		";
	return innerHTML;
}
function createInpunt(label, name, placeholder, value){
	var input = "<div>\
    				<label class='label-exc' for='" + name + "''>" + label + "</label>\
					<input id='" + name + "' class='input-exc' type='text' name='" + name + "' value='" + value + "' placeholder='" + placeholder + "'/>\
				</div>";
	return input
}

function createTextArea(label, name, placeholder, value){
	var textarea = "<div>\
    				<label class='label-exc' for='" + name + "''>" + label + "</label>\
    				<textarea id='" + name + "' class='textarea-exc' cols='40' rows='6' name='" + name + "' value='" + value + "'></textarea>\
				</div>";
	return textarea
}

function createSelect(name, label, options, id, _class){
	var opt = '<option>Select one option</option>';
	for (var i = 0; i < options.length; i++) {
		opt = opt + '<option value=' + options[i].value + '>' + options[i].label + '</option>';
	};
	var select = '<div>\
    				<label class=' + _class + ' for=' + name + '>' + label + '</label>\
    				<select id=' + id + ' class="selection-exc" name=' + name + '>' + opt + '</select>\
				</div>';
	return select;
}

function eventClickButton(){
	return 'alert("I was clicked");'
}

function getStyle(){
	var style = "<style>\
					.selection-exc {\
						outline: none !important;\
					  	padding: 3px 0px 3px 3px !Important;\
					  	margin: 0px 1px 3px 0px !Important;\
					  	border: 1px solid #DDDDDD !Important;\
					  	width: 262px;margin-left: 4% !Important;\
					  	background: white !Important;\
					}\
					.container-exc {\
						padding-left: 12px !Important;\
						padding-right: 10px !Important;\
						width:300px !Important;\
						height: 100% !Important;\
						background:#fafafa !Important;\
						margin-left: 0% !Important;\
						margin-top: 0% !Important;\
						border: 0px solid black !Important;\
						overflow-y: scroll !Important;\
					}\
					.menu-exc {\
						padding: 0px 5px 0px 12px !important;\
						background: rgba(14,78,90,0.9) !important;\
						width:100%% !important;\
						border: 0px solid black !important;\
						margin-top: 0% !important;\
						height: 120px !Important;\
						border-bottom: 1px solid #DDDDDD !Important;\
					}\
					.sideBarToggle-exc {\
						height: 20px !important;\
						margin-left: 1% !important;\
						top: 12px !important;\
						position: absolute !important;\
						margin-top: 1% !Important;\
					}\
					.menuBarToggle-exc {\
						height: 15px !important;\
						margin-left: 0% !important;\
						margin-top: -5px !important;\
						position: relative !important;\
						float: left !important;\
						margin-right: 7px !Important;\
					}\
					.header-exc {\
						width: 100% !important;\
						padding-bottom: 26px !important;\
						padding-top: 24px !important;\
						padding-left: 90% !important;\
						border: 0px solid gray !Important;\
						background:#0d4651 !Important;\
					}\
					.menu-label-exc {\
						margin-left: 4% !Important;\
						margin-bottom: 0px !Important;\
						margin-top: 3% !Important;\
						float: left !Important;\
						color: white !Important;\
						font-weight: 500 !Important;\
					}\
					.label-exc {\
					  margin-left: 4% !Important;\
					  margin-bottom: 0px !Important;\
					  margin-top: 3% !Important;\
					  float: left !Important;\
					}\
					label {\
					    display: inline-block !Important;\
					    max-width: 100% !Important;\
					    margin-bottom: 5px !Important;\
					    font-weight: bold !Important;\
					}\
					.input-exc:focus, .textarea-exc:focus {\
					    box-shadow: 0 0 5px rgba(153, 189, 223, 1)!Important;\
					    padding: 3px 0px 3px 3px !Important;\
					    border: 1px solid rgba(153, 189, 223, 1) !Important;\
					}\
					.empty-exc {\
					    box-shadow: 0 0 5px rgba(221, 75, 57, 1)!Important;\
					    padding: 3px 0px 3px 3px !Important;\
					    border: 1px solid rgba(221, 75, 57, 1) !Important;\
					}\
					.input-exc, .textarea-exc {\
					  -webkit-transition: all 0.30s ease-in-out !Important;\
					  -moz-transition: all 0.30s ease-in-out !Important;\
					  -ms-transition: all 0.30s ease-in-out !Important;\
					  -o-transition: all 0.30s ease-in-out !Important;\
					  outline: none !Important;\
					  padding: 3px 0px 3px 3px !Important;\
					  margin: 0px 1px 3px 0px !Important;\
					  border: 1px solid #DDDDDD;\
					  width: 260px;margin-left: 4% !Important;\
					}\
					.button-exc {\
					    display: inline-block !Important;\
					    padding: 3px 20px !Important;\
					    margin: 15px 12px 250px 12px !Important;\
					    font-size: 14px !Important;\
					    cursor: pointer !Important;\
					    text-align: center !Important;\
					    text-decoration: none !Important;\
					    outline: none !Important;\
					    color: #fff !Important;\
					    background-color: #00BCD4 !Important;\
					    border: none !Important;\
					    border-radius: 4px !Important;\
					    box-shadow: 0 2px #CCC !Important;\
					    width: 260px!Important;\
					}\
					.button-exc:hover{background-color: #00c0ef !Important;}\
					.button-exc:active {\
					  	background-color: #00c0ef !Important;\
					 	box-shadow: 0 2px #CCC !Important;\
					  	transform: translateY(1px) !Important;\
					}\
					body{\
						font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif !Important;\
						font-size: 14px !Important;\
					    line-height: 1.42857143 !Important;\
					    color: #333333 !Important;\
					}\
					#mySidebar {\
						position:fixed !Important;\
						top:0px !Important;\
						right:0px !Important;\
						width:300px !Important;\
					    height:100% !Important;\
					    transform: translateX(0px);\
						background:aliceblue !Important;\
						border:0px solid black !Important;\
						z-index:999999;\
						transition: transform 1s ease-in-out;\
						-webkit-box-shadow: -4px 0px 21px -7px rgba(0,0,0,0.75) !Important;\
						-moz-box-shadow: -4px 0px 21px -7px rgba(0,0,0,0.75) !Important;\
						box-shadow: -4px 0px 21px -7px rgba(0,0,0,0.75) !Important;\
					}\
				</style>";
	return style;
}